#!/usr/bin/perl
use strict;
use warnings;

use LWP::UserAgent;
use JSON::PP qw(decode_json);
use Cwd;
use File::Basename;

my $scriptDir = dirname(__FILE__);
my $confFile = "daemonitors.conf";

my $config;

sub load_config {
    my %s_config = ();
    open(CONF, "<", "$scriptDir/$confFile") or do { print "Failed, cannot open configuration file\n"; return 0; };

    foreach my $line(<CONF>){
        chomp($line);
        my ($left, $right) = split('=',$line);
        $left =~ s/\s//;
        $s_config{$left} = $right;
    }

    if(!%s_config){
        print "Failed, configuration file has no contents\n";
        return 0;
    }

    $config = \%s_config;

    return 1;
}

sub doCheck {
    my %daemon = ();

    my @arrDaemon = split(/\,/, $config->{daemon});
    foreach (@arrDaemon){
        my ($daemonName, $daemonBin) = split /\:/, $_;
        $daemonName =~ s/^\s+//;
        $daemon{$daemonName} = $daemonBin;
    }

    my %daemonStatus = ();

    my @email_receiver_list = split(/\,/,$config->{emailReceiver});

    foreach (@email_receiver_list) {
        $_ = '"'.$_.'"';
    }

    my $notActive = 0;

    foreach my $key(sort keys %daemon){
        my $a = `pgrep -f $daemon{$key}`;
        if ($a) {
            $daemonStatus{$key} = "Active";
            next;
        }

        $daemonStatus{$key} = "Not Active";
        $notActive++;
    }

    if ($notActive > 0) {
        my $emailMessage = "
        <p>Dear Rekan,</p>
        <p>Dari hasil pengecekan kami, terdapat daemon yang mati/tidak aktif pada server <b>Recon Roaming</b>. Berikut tabel status daemon :</p>
        <table border='1' style='border:solid 1px #000; border-collapse:collapse;font:14px Arial,Helvetica,sans-serif;'>
        <thead>
            <tr style='background:#e8423b;'>
                <td style='padding: 2px 5px 2px 5px; color:#fff; border-color:#000; font-weight:bold;'>Daemon</td>
                <td style='padding: 2px 5px 2px 5px; color:#fff; border-color:#000; font-weight:bold;'>Status</td>
            </tr>
        </thead>
        <tbody>";

        foreach my $key(sort keys %daemonStatus){
            my $color = "#e8423b";
            if ($daemonStatus{$key} eq "Active"){
                $color = "#329932";
            }
            $emailMessage .= "<tr>
                <td style='padding: 2px 5px 2px 5px; border-color:#000;'>$key</td>
                <td style='padding: 2px 5px 2px 5px; color:".$color."; border-color:#000;'><b>$daemonStatus{$key}</b></td>
                </tr>";
        }
        
        $emailMessage .= "
        </tbody>
        </table>
        <p>Mohon agar dilakukan pengecekan terhadap daemon tidak aktif tersebut.</p>
        <p>Terima Kasih.</p>";

        $emailMessage =~ s/\n//g;

        my $email_receiver = join(",",@email_receiver_list);

        my $jsonReqBody = '{
            "email_receiver": ['.$email_receiver.'],
            "email_title": "[ Alert ] Daemon Mati",
            "email_subject": "Daemon Mati/Tidak Aktif",
            "email_message": "'.$emailMessage.'"
        }';
            
        my $ua = LWP::UserAgent->new;
        $ua->agent("MyApp/0.1 ");

        my $req = HTTP::Request->new(POST => "http://localhost:5000/sendMail");
        $req->content_type('application/json');
        $req->content($jsonReqBody);
        
        my $res = $ua->request($req);
        
        # Check the outcome of the response
        if (!$res->is_success) {
            print "Failed, cannot request send alert\n";
        }
    }
}

sub main {
    if(!-e "$scriptDir/$confFile"){
        print "$scriptDir/$confFile not exists, daemon terminated\n";
        exit;
    }
    my $status = load_config();
    doCheck() if $status;
}

main();